module TerrariaBestCombinations


using TOML
export Preference
export Biome
export Npc
export Neighbor

include("./biomes.jl")
include("./npcs.jl")
include("./utils.jl")

end # module TerrariaBestCombinations
