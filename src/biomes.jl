"""
  `Preference`

Abstract supertype of all preferences.
"""
abstract type Preference end

mutable struct Biome <: Preference
    name::String
    repr_color::String
    repr_priority::Int64
    hybrid_allowedlist::Vector{Biome}
    hybrid_blockedlist::Vector{Biome}
    function Biome(n::String, rc::String, rp::Int64, ha::Vector{Biome}, hb::Vector{Biome})
        new(n, rc, rp, Biome[ha...], Biome[hb...])
    end
end

Biome() = Biome("", "", 0, Vector{Biome}(undef, 0), Vector{Biome}(undef, 0))


