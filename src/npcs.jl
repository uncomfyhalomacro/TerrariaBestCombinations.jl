"""
  `Npc <: Preference`

An `Npc` is a world character. It is also a subtype of `Preference` since
they may like or dislike a `Biome` or another `Npc`.

# Examples

You can declare an `Npc` like the following.


```jldoctest
julia> x = Npc() # Creates an Npc with nothing on it
Npc(:npc, "", "", true, Preference[])

julia> Npc("lumberjack", "woodworker", true, x)
```

"""
mutable struct Npc <: Preference
    name::String
    pretty_name::String
    happiness_effect::Bool
    loved::Vector{T} where {T<:Preference}
    liked::Vector{T} where {T<:Preference}
    disliked::Vector{T} where {T<:Preference}
    hated::Vector{T} where {T<:Preference}
    function Npc(n::String, pn::String, he::Bool, loved::Vector{T}, liked::Vector{T}, disliked::Vector{T}, hated::Vector{T}) where {T<:Preference}
        new(n, pn, he, Preference[loved...], Preference[liked...], Preference[disliked...], Preference[hated...])
    end
end

Npc() = Npc("", "", false,
    Vector{Preference}(undef, 0),
    Vector{Preference}(undef, 0),
    Vector{Preference}(undef, 0),
    Vector{Preference}(undef, 0),
)
Neighbor() = Npc()


